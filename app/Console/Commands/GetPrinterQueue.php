<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Integrations\CRM\CrmConnectorContract;
use Illuminate\Console\Command;

class GetPrinterQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get-printer-queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /** Execute the console command. */
    public function handle(CrmConnectorContract $crmConnector): void
    {
        $crmConnector->getPrinterQueue();
    }
}

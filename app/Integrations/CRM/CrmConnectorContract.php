<?php

declare(strict_types=1);

namespace App\Integrations\CRM;

use Illuminate\Contracts\Foundation\Application;

interface CrmConnectorContract
{
    public function getPrinterQueue(): mixed;

    public static function register(Application $application): void;
}

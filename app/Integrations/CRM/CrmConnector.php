<?php

declare(strict_types=1);

namespace App\Integrations\CRM;

use App\Integrations\CRM\Requests\GetPrinterQueue;
use Illuminate\Contracts\Foundation\Application;
use JsonException;
use ReflectionException;
use Saloon\Http\Connector as SaloonConnector;
use Saloon\Traits\Plugins\AcceptsJson;
use Saloon\Traits\Plugins\AlwaysThrowOnErrors;
use Throwable;

class CrmConnector extends SaloonConnector implements CrmConnectorContract
{
    use AcceptsJson;
    use AlwaysThrowOnErrors;

    /** @return string */
    public function resolveBaseUrl(): string
    {
        return config('services.crm.host');
    }

    /**
     * @return mixed|array
     * @throws JsonException
     * @throws ReflectionException
     * @throws Throwable
     */
    public function getPrinterQueue(): mixed
    {
        return $this
            ->send(new GetPrinterQueue())
            ->json();
    }

    /**
     * @param  Application  $application
     *
     * @return void
     */
    public static function register(Application $application): void
    {
        $application
            ->singleton(
                abstract: CrmConnectorContract::class,
                concrete: fn (): CrmConnectorContract => new self(),
            );
    }
}

<?php

declare(strict_types=1);

namespace Deployer;

require 'recipe/laravel.php';

// Config

set('repository', 'git@gitlab.com:kodeksoff/printserver.git');

set('bin/php', 'cd {{release_or_current_path}}/_docker && docker-compose exec -T workspace bash -c php artisan');
set('bin/composer', 'cd {{release_or_current_path}}/_docker && docker-compose exec -T workspace bash -c composer');

set('shared_dirs', ['storage']);
set('shared_files', [
    '_docker/.env',
    '.env',
]);
set('writable_dirs', [
    'bootstrap/cache',
    'storage',
    'storage/app',
    'storage/app/public',
    'storage/framework',
    'storage/framework/cache',
    'storage/framework/cache/data',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
]);
set('log_files', 'storage/logs/*.log');

// Hosts
host('agrarnaya')
    ->set('hostname', '92.255.204.35')
    ->set('port', '22034')
    ->set('remote_user', 'root')
    ->set('deploy_path', '/_code/printserver');

desc('Installs vendors');
task('deploy:vendors', function (): void {
    run('{{bin/composer}} {{composer_action}} {{composer_options}} 2>&1');
});

desc('Restart docker');
task('deploy:restart_docker', function (): void {
    cd('{{release_or_current_path}}/_docker');
    run('docker-compose restart');
});

after('deploy:writable', 'deploy:restart_docker');

// Hooks

after('deploy:failed', 'deploy:unlock');
